package view;

import model.domain.train.Train;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Scanner;

public class ConsoleView {

    public static final String NO_DATA_FOUND = "NO DATA FOUND";

    public static void print(LinkedHashMap menu) {
        System.out.println();
        for (int i = 1; i < menu.size(); i++) {
            System.out.println("[" + i + "] " + menu.get(Integer.toString(i)));
        }
        System.out.println("[q] " + menu.get("q"));
    }

    public static void print(LinkedList<Train> trains) {
        System.out.println();
        for (Train t : trains) {
            System.out.println(t.getName());
        }
    }

    public static void printRequest(String request) {
        System.out.print("\n" + request + ": ");
    }

    public static void printMessage(String message) {
        System.out.println("\n" + message);
    }

    public static String getUserInput() {
        return new Scanner(System.in).next();
    }

    public static String getUserMenuInput() {
        ConsoleView.printRequest("Your choice");
        return new Scanner(System.in).next();
    }


}
