package model.exeption;

public class TrainNotFoundException extends TrainException {

  public TrainNotFoundException() {
  }

  public TrainNotFoundException(String message) {
    super(message);
  }

  public TrainNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  public TrainNotFoundException(Throwable cause) {
    super(cause);
  }
}
