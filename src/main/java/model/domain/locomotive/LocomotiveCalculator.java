package model.domain.locomotive;

import model.domain.carriage.Carriage;

import java.util.List;

public interface LocomotiveCalculator {

  boolean canCarry(List<Carriage> carriages);

  float getMaxSpeedWithLoad(List<Carriage> carriages);
}
