package model.domain.train;

public interface TrainCalculator {

  boolean canCarryAllPassengers();

  boolean canCarryAllGoods();

  void sortCarriagesByComfortLevel();

  float calculateWholeCargoWeight();
}
