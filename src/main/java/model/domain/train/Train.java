package model.domain.train;

import model.domain.carriage.Carriage;
import model.domain.carriage.FreightCarriage;
import model.domain.carriage.PassengersCarriage;
import model.domain.locomotive.Locomotive;

import java.util.LinkedList;

public class Train implements TrainCalculator {

  private String name;
  private Locomotive locomotive;
  private LinkedList<Carriage> carriages;
  private float goodsWeigh;
  private int passengersNumber;

  public String getName() {
    return name;
  }

  public void setLocomotive(Locomotive locomotive) {
    this.locomotive = locomotive;
  }

  public void setCarriages(LinkedList<Carriage> carriages) {
    this.carriages = carriages;
  }

  public Locomotive getLocomotive() {
    return locomotive;
  }

  public LinkedList<Carriage> getCarriages() {
    return carriages;
  }

  public float getGoodsWeigh() {
    return goodsWeigh;
  }

  public int getPassengersNumber() {
    return passengersNumber;
  }

  private Train(TrainBuilder trainBuilder) {
    this.name = trainBuilder.name;
    this.locomotive = trainBuilder.locomotive;
    this.carriages = trainBuilder.carriages;
    this.goodsWeigh = trainBuilder.goodsWeigh;
    this.passengersNumber = trainBuilder.passengersNumber;
  }

  public boolean canCarryAllPassengers() {
    int availablePlace = 0;
    for (Carriage c : this.carriages) {
      availablePlace += (c instanceof PassengersCarriage) ? c.getCarryingCapacity() : 0;
    }
    return this.passengersNumber <= availablePlace;
  }

  public boolean canCarryAllGoods() {
    int availableSpaceForGoods = 0;
    for (Carriage c : this.carriages) {
      availableSpaceForGoods += (c instanceof FreightCarriage) ? c.getCarryingCapacity() : 0;
    }
    return this.goodsWeigh <= availableSpaceForGoods;
  }

  public void sortCarriagesByComfortLevel() {
    if (this.carriages != null) {
      sortAllFreightCarriagesToBack();
      for (int i = 0; i < this.carriages.size() - 1; i++) {
        for (int j = 0; j < this.carriages.size() - 1; j++) {
          Carriage c = this.carriages.get(j);
          if ((c instanceof PassengersCarriage) &&
              (this.carriages.get(j + 1) instanceof PassengersCarriage)) {
            if (((PassengersCarriage) c).getPassengersCarriageType()
                .compareTo(((PassengersCarriage) this.carriages.get(j + 1))
                    .getPassengersCarriageType()) > 0) {
              this.carriages.set(j, this.carriages.get(j + 1));
              this.carriages.set(j + 1, c);
            }
          }
        }
      }
    }
  }

  private void sortAllFreightCarriagesToBack() {
    for (int i = 0; i < this.carriages.size() - 1; i++) {
      for (int j = 0; j < this.carriages.size() - i; j++) {
        Carriage c = this.carriages.get(j);
        if ((c instanceof FreightCarriage) &&
            (this.carriages.get(j + 1) instanceof PassengersCarriage)) {
          this.carriages.set(j, this.carriages.get(j + 1));
          this.carriages.set(j + 1, c);
        }
      }
    }
  }

  public float calculateWholeCargoWeight() {
    return this.goodsWeigh + this.passengersNumber * 62;
  }

  public static class TrainBuilder {

    private String name;
    private Locomotive locomotive;
    private LinkedList<Carriage> carriages = new LinkedList<Carriage>();
    private float goodsWeigh;
    private int passengersNumber;

    public TrainBuilder setName(String name) {
      this.name = name;
      return this;
    }

    public TrainBuilder setLocomotive(Locomotive locomotive) {
      this.locomotive = locomotive;
      return this;
    }

    public TrainBuilder setCarriages(LinkedList<Carriage> carriages) {
      this.carriages = carriages;
      return this;
    }

    public TrainBuilder setGoodsWeigh(float goodsWeigh) {
      this.goodsWeigh = goodsWeigh;
      return this;
    }

    public TrainBuilder setPassengersNumber(int passengersNumber) {
      this.passengersNumber = passengersNumber;
      return this;
    }

    public Train build() {
      return new Train(this);
    }
  }
}
