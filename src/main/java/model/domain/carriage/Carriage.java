package model.domain.carriage;

public abstract class Carriage {

  private int carryingCapacity;
  private int weigh;

  public Carriage(int carryingCapacity, int weigh) {
    this.carryingCapacity = carryingCapacity;
    this.weigh = weigh;
  }

  public int getCarryingCapacity() {
    return carryingCapacity;
  }

  public int getWeigh() {
    return weigh;
  }
}
