package model.domain.carriage;

public class FreightCarriage extends Carriage {

  private FreightCarriageType freightCarriageType;

  public FreightCarriage(int carryingCapacity, int weigh, String freightCarriageType) {
    super(carryingCapacity, weigh);
    this.freightCarriageType = FreightCarriageType.
        valueOf(freightCarriageType.toUpperCase().trim());
  }

  public FreightCarriageType getFreightCarriageType() {
    return freightCarriageType;
  }
}
