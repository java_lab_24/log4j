package model.domain.carriage;

public class PassengersCarriage extends Carriage {

  private PassengersCarriageType passengersCarriageType;

  public PassengersCarriage(int carryingCapacity, int weigh, String passengersCarriageType) {
    super(carryingCapacity, weigh);
    this.passengersCarriageType = PassengersCarriageType.
        valueOf(passengersCarriageType.toUpperCase().trim());
  }

  public PassengersCarriageType getPassengersCarriageType() {
    return passengersCarriageType;
  }
}
