import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class LogTest {

  private static Logger logger = LogManager.getLogger(LogTest.class);

  public static void main(String[] args) {
    logger.trace("log msg");
    logger.debug("log msg");
    logger.info("log msg");
    logger.warn("log msg");
    logger.error("log msg");
    logger.fatal("log msg");
  }

}
